package contacts.service;

public class DialogFactory {
    public Dialog getDialog(String contact){
        if(contact.equals("person")){
            return new PersonDialog();
        } else {
            return new OrganizationDialog();
        }
    }
}
