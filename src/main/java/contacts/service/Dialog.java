package contacts.service;

public interface Dialog {
    void create();
    void edit();
    void read();
}
