package contacts.service2;

public abstract class AbstractDialog2 {

    public void create(){
        //do smth common
        //...
        //call dialog method
        dialog().create();
    }

    public void read(){

        //do smth common
        //...
        //call dialog method
        dialog().read();
    }
    public abstract Dialog2 dialog();
}
