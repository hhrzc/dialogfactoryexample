package contacts;

import contacts.service.Dialog;
import contacts.service.DialogFactory;

public class Main {
    public static void main(String[] args) {
        String contact = getContact();
        Dialog dialog = new DialogFactory().getDialog(contact);
        String command = getCommand();
        executeCommand(command, dialog);
    }

    private static void executeCommand(String command, Dialog dialog) {
        switch (command){
            case "read":
                dialog.read();
            case "create":
                dialog.create();
//                ...
        }
    }

    private static String getCommand() {
        //read from console and get command ('read'/'create'/...)
        return null;
    }

    private static String getContact() {
        //read from console and get parameter 'person'/'organization'
        return null;
    }
}
