package contacts;

import contacts.service.Dialog;
import contacts.service.DialogFactory;
import contacts.service2.AbstractDialog2;
import contacts.service2.Dialog2;
import contacts.service2.PersonDialogFactory2;
import jdk.jshell.spi.ExecutionControl;

public class Main2 {
    public static void main(String[] args) {
        String contact = getContact();
        AbstractDialog2 dialog = configureDialog(contact);
        String command = getCommand();
        executeCommand(command, dialog);
    }

    private static AbstractDialog2 configureDialog(String contact) {
        switch (contact){
            case "person":
                return new PersonDialogFactory2();
        }
        throw new RuntimeException("Not implemented");
    }

    private static String getCommand() {
        //read from console and get command ('read'/'create'/...)
        return null;
    }

    private static String getContact() {
        //read from console and get parameter 'person'/'organization'
        return null;
    }

    //You can change to the enum implementation
    private static void executeCommand(String command, AbstractDialog2 dialog) {
        switch (command){
            case "read":
                dialog.read();
            case "create":
                dialog.create();
//                ...
        }
    }
}
